@extends('layouts.main')

@section('content')
    <div style="margin-left: 75px">
        <div class="invoice p-3 mb-3">

            <!-- title row -->
            <div class="row">
                <div class="col-12">
                    <h4>
                        <i class="fas fa-globe"></i> {{ $land->libelle }}
                        <small class="float-right">{{ $land->created_at->diffForHumans() }}</small>
                    </h4>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    {{ $land->continent }}
                    <address>
                        <strong>Description</strong><br>
                        {{ $land->description }}
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>Indicatif</strong><br>
                        {{ $land->code_indicatif }}<br>
                        <strong>Superficie</strong><br>
                        {{ $land->superficie }}<br>
                        <strong>Population</strong><br>
                        {{ $land->population }} habitants
                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                    <b>ID: </b>{{ $land->id }}<br>
                    <br>
                    <b>Monnaie: </b>{{ $land->monnaie }}<br>
                    <b>Langue: </b>{{ $land->langue }}<br>
                    <b>Laique: </b>{{ $land->est_laique ? 'Oui' : 'Non' }}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->


            <div class="row no-print">
                <div class="col-12">
                    <a href="{{ route('lands.index') }}" rel="noopener"
                        class="btn btn-default">Retour</a>
                    
                </div>

            </div>
        </div>
    </div>
@endsection
