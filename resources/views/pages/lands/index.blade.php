@extends('layouts.main')

@section('content')
    <div style="margin-left: 5%">

        <div class="card">
            <div class="card-header">
                <table>
                    <tr>
                        <td><h3 class="card-title">Tableau des Lands </h3></td>
                        <td><a href="{{ route('lands.create')}}"><button type="button" class="btn btn-block btn-primary btn-sm">Ajouter un Land</button></a></td>
                    </tr>
                </table>
            </div>
            <!-- /.card-header -->
            <div class="card-body ml-10">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Libelle</th>
                            <th>Description</th>
                            <th>Indicatif</th>
                            <th>Continent</th>
                            <th>Population</th>
                            <th>Capitale</th>
                            <th>Monnaie</th>
                            <th>Langue</th>
                            <th>Superficie</th>
                            <th>Laique</th>
                            <th>Act</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lands as $land)
                            <tr>
                                <td>{{ $land->id }}</td>
                                <td>{{ $land->libelle }}</td>
                                <td>{{ $land->description }}</td>
                                <td>{{ $land->code_indicatif }}</td>
                                <td>{{ $land->continent }}</td>
                                <td>{{ $land->population }}</td>
                                <td>{{ $land->capitale }}</td>
                                <td>{{ $land->monnaie }}</td>
                                <td>{{ $land->langue }}</td>
                                <td>{{ $land->superficie }}</td>
                                <td>{{ $land->est_laique ? 'Oui' : 'Non' }}</td>
                                <td>
                                    <div class="nav-item dropdown">
                                        <a class="nav-link bg-danger dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">...</a>
                                        <div class="dropdown-menu mt-0" style="left: 0px; right: inherit;">
                                          <a class="dropdown-item" href="{{ route('lands.show', ['id' => $land->id]) }}" data-widget="iframe-close" data-type="all">Afficher</a>
                                          <a class="dropdown-item" href="{{ route('lands.edit', ['id' => $land->id]) }}" data-widget="iframe-close" data-type="all-other">Modifier</a>
                                          <a class="dropdown-item" href="{{ route('lands.destroy', ['id' => $land->id]) }}" data-widget="iframe-close" data-type="all-other">Supprimer</a>
                                        </div>
                                      </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>


    </div>
@endsection
