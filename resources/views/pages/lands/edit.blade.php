@extends('layouts.main')

@section('content')
    <div style="margin-left: 5%">
        <div class="card-header">
            <h1>Modifier le Land</h1>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div>
            <form class="form-horizontal" method="POST" action="{{ route('lands.update', ['id' => $land->id]) }}">
                @csrf
                @method('POST')
                <div class="card-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Libelle</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="Libelle" name="libelle" value="{{ $land['libelle'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPassword3" placeholder="Description" name="description" value="{{ $land['description'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Indicatif</label>
                        <div class="col-sm-10">
                            <table>
                                <tr>
                                    <td><input type="text" class="form-control" id="inputPassword3" name="code_indicatif" value="{{ $land['code_indicatif'] }}"></td>
                                </tr>
                            </table>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Continent</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="continent">
                                <option style="color: blue">{{$land['continent']}}</option>
                                <option  disabled>--------</option>
                                <option>Afrique</option>
                                <option>Amerique</option>
                                <option>Antarctique</option>
                                <option>Asie</option>
                                <option>Europe</option>
                                <option>Océanie</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Population</label>
                        <div class="col-sm-10">
                            <table>
                                <tr>
                                    <td><input type="number" class="form-control" id="inputPassword3" placeholder="XXXXXXXXXX" name="population" value="{{ $land['population'] }}"></td>
                                    <td>habitants</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Capitale</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="inputPassword3" placeholder="Capitale" name="capitale" value="{{ $land['capitale'] }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Monnaie</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="monnaie" value="{{ $land['monnaie'] }}">
                                <option style="color: blue">{{$land['monnaie']}}</option>
                                <option  disabled>--------</option>
                                <option>XOF</option>
                                <option>EUR</option>
                                <option>DOLLAR</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Langue</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="langue" value="{{  $land['langue'] }}">
                                <option style="color: blue">{{$land['langue']}}</option>
                                <option  disabled>--------</option>
                                <option>FR</option>
                                <option>EN</option>
                                <option>AR</option>
                                <option>ES</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Superficie</label>
                        <div class="col-sm-10">
                            <table>
                                <tr>
                                    <td><input type="number" class="form-control" id="inputPassword3" placeholder="XXXXXXXXXX" name="superficie" value="{{ $land['superficie'] }}"></td>
                                    <td>km2</td>
                                </tr>
                            </table>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Laique</label>
                        <div class="col-sm-10">
                            <div class="col-sm-6">

                                <div class="form-group" style="margin-left: 130px">
                                    <div class="form-group">
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="est_laique"  id="true" value='1'>
                                          <label for="true" class="form-check-label">Oui</label>
                                        </div>
                                        <div class="form-check">
                                          <input class="form-check-input" type="radio" name="est_laique" id="false" value='0'>
                                          <label for="false" class="form-check-label">Non</label>
                                        </div>
                              </div>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Ajouter</button>
                </div>
                <!-- /.card-footer -->
            </form>
        </div>
    </div>

@endsection
