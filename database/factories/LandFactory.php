<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $continent =  ["Afrique", "Amerique", "Europe", "Asie", "Oceanie","Antarctique"];
        $monnaie = ["XOF", "EUR", "DOLLAR"];
        $langue = ["FR", "EN", "AR", "ES"];
        return [
            "libelle" => $this->faker->word(),
            "description" => $this->faker->paragraph(),
            "code_indicatif" => $this->faker->numerify('+####'),
            "continent" =>$continent[array_rand($continent)],
            "population"=> rand(1000,2000000000),
            "capitale" => $this->faker->word(),
            "monnaie"=> $monnaie[array_rand($monnaie)],
            "langue" => $langue[array_rand($langue)],
            "superficie" => rand(1000,20000000),
            "est_laique"=> $this->faker->boolean(),
        ];
    }
}


