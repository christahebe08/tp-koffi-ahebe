<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Land;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lands = Land::all();
        return view ('pages.lands.index', ["lands" => $lands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.lands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $request -> validate([
            "libelle"=> "required",
            "description" => "required",
            "code_indicatif" => "required|unique:lands,code_indicatif",
            "continent" => "required",
            "population" => "required",
            "capitale" => "required|unique:lands,capitale",
            "monnaie" => "required",
            "langue" => "required",
            "superficie" => "required",
            "est_laique" => "required",
        ]);
        //enregistrement de land dans la bd
        Land::create([
            "libelle"=> $request->get("libelle"),
            "description" => $request->get("description"),
            "code_indicatif" => "+".$request->get("code_indicatif"),
            "continent" => $request->get("continent"),
            "population"=> $request->get("population"),
            "capitale" => $request->get("capitale"),
            "monnaie"=> $request->get("monnaie"),
            "langue" => $request->get("langue"),
            "superficie" => $request->get("superficie"),
            "est_laique"=> $request->get("est_laique"),

        ]);

        return redirect()->route('lands.index');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $land = Land::FindOrFail($id);
        return view("pages.lands.show", ["land" => $land]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $land = Land::FindOrFail($id);
        return view("pages.lands.edit", ["land" => $land]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            "libelle"=> "required",
            "description" => "required",
            "code_indicatif" => "required",
            "continent" => "required",
            "population" => "required",
            "capitale" => "required",
            "monnaie" => "required",
            "langue" => "required",
            "superficie" => "required",
            "est_laique" => "required",
        ]);

        Land::whereId($id)->update($validatedData);
        return redirect('/lands');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function destroy($id)
    {
        $land = Land::FindOrFail($id);
        $land->delete();

        return redirect('/lands');
    }
}
